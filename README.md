# ADSimDetector conda recipe

Home: "https://github.com/areaDetector/ADSimDetector"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ADSimDetector module
